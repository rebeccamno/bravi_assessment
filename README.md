
**Bravi- Back End - Technical Assessment**

This application  allows the registration of chess pieces  via admin interface (/admin).
In addition, given a  coordinate in algebraic notation chosen by the user and the piece id, returns all possible locations where the knight can move in 2 turns.

##  Instalation

  Clone this repository.   You need to have `Python 3.8` installed.

  Or you can also run with docker  :
  ```docker-compose up```



##   Run the app
To initialize this project  for the first time you need to run
```
make migrate
```
then

```
make run
```

## Run the tests
```
make tests
```

## Tools
The following tools were used in building the project:
- [Django](https://www.djangoproject.com/)
- [Django Rest Framework](https://www.django-rest-framework.org/)



## Admin (/admin)
Django admin interface
for test purpose:
```
	user : admin
	password: admin
```

 - Chess Piece CRUD

## API


The REST API is described below.

## Get piece id by type and color
Receive the piece type and the color and returns the piece id

### Request

`GET /api/v1/chess-piece/find-id/?piece_type={piece_type}&color={color}/`

    curl -i -H 'Accept: application/json' "http://0.0.0.0:8000/api/v1/chess-piece/find-id/?piece_type=knight&color=dark"

### Response

    HTTP/1.1 200 OK
	Date: Sun, 06 Mar 2022 23:42:42 GMT
	Server: WSGIServer/0.2 CPython/3.8.12
	Content-Type: application/json
	Vary: Accept, Cookie
	Allow: GET, HEAD, OPTIONS
	X-Frame-Options: DENY
	Content-Length: 8
	X-Content-Type-Options: nosniff
	Referrer-Policy: same-origin
	Cross-Origin-Opener-Policy: same-origin


	{ "id":2}

 ## Get a non-existent piece
### Request

`GET /api/v1/chess-piece/find-id/?piece_type={piece_type}&color={color}/`

    curl -i -H 'Accept: application/json' "http://0.0.0.0:8000/api/v1/chess-piece/find-id/?piece_type=knight&color=blue"

### Response

    TTP/1.1 404 Not Found
	Date: Sun, 06 Mar 2022 23:52:38 GMT
	Server: WSGIServer/0.2 CPython/3.8.12
	Content-Type: application/json
	Vary: Accept, Cookie
	Allow: GET, HEAD, OPTIONS
	X-Frame-Options: DENY
	Content-Length: 23
	X-Content-Type-Options: nosniff
	Referrer-Policy: same-origin
	Cross-Origin-Opener-Policy: same-origin

	{"detail":"Not found."}




## Get piece possible locations
Receive cell coordinate (in Algebraic notation) and the piece id and return all possible locations where the knight can move within 2 turns.
### Request

`GET /api/v1/chess-piece/get-possible-locations/2/?coordinate=e5`

    curl -i -H 'Accept: application/json' "http://0.0.0.0:8000/api/v1/chess-piece/get-possible-locations/2/?coordinate=e5"

### Response

    HTTP/1.1 200 OK
	Date: Sun, 06 Mar 2022 23:59:18 GMT
	Server: WSGIServer/0.2 CPython/3.8.12
	Content-Type: application/json
	Vary: Accept, Cookie
	Allow: GET, HEAD, OPTIONS
	X-Frame-Options: DENY
	Content-Length: 353
	X-Content-Type-Options: nosniff
	Referrer-Policy: same-origin
	Cross-Origin-Opener-Policy: same-origin

	[{"g6":["e7","e5","f8","h8","f4","h4"]},{"g4":["e5","e3","f6","h6","f2","h2"]},{"c6":["e7","e5","a7","a5","b8","d8","b4","d4"]},{"c4":["e5","e3","a5","a3","b6","d6","b2","d2"]},{"d7":["f8","f6","b8","b6","c5","e5"]},{"f7":["h8","h6","d8","d6","e5","g5"]},{"d3":["f4","f2","b4","b2","c5","e5","c1","e1"]},{"f3":["h4","h2","d4","d2","e5","g5","e1","g1"]}]
	 
