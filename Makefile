tests:
	python3 -m pytest -s -vv -m "not slow" --cov-config=.coveragerc --cov=tests --cov=chess_board --cov=chess_piece -W ignore::DeprecationWarning

run:
	python3 manage.py runserver

migrate:
	python3 manage.py migrate

migrations:
	python3 manage.py makemigrations
