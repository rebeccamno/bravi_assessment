from chess_piece.models import ChessPiece
from chess_piece.pieces import Knight


class CheckPossibleLocation:

    def __init__(self, chess_board, chess_piece, algebraic_coordinate):
        self.chess_board = chess_board
        self.piece_type = self.get_piece_type(chess_board, chess_piece)
        self.coordinate = chess_board.convert_algebric_to_coordinate(
                            algebraic_coordinate)

    @staticmethod
    def get_piece_type(chess_board, chess_piece):
        pieces = {
            ChessPiece.PieceType.KNIGHT: Knight(chess_board)
        }
        return pieces.get(chess_piece.piece_type)

    def calculate_possible_location(self, coordinate):
        positions = self.piece_type.calculate_possible_location(
                    coordinate)
        algebraic_positions = []

        for position in positions:
            algebraic_position = self.chess_board \
                                    .get_algebraic_position(position)
            algebraic_positions.append(algebraic_position)

        return algebraic_positions

    def __call__(self):
        algebraic_positions = self.calculate_possible_location(self.coordinate)
        result = algebraic_positions

        for index, item in enumerate(result):
            converted_pos = self.chess_board.convert_algebric_to_coordinate(
                                item)
            al_positions = self.calculate_possible_location(converted_pos)
            result[index] = {item: al_positions}

        return result
