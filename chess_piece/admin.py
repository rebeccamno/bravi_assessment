from django.contrib import admin
from .models import ChessPiece


admin.site.register(ChessPiece)
