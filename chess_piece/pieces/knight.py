

class Knight:
    # X Y
    possible_moves = [
        [2, 1],
        [2, -1],
        [-2, 1],
        [-2, -1],
        [-1, 2],
        [1, 2],
        [-1, -2],
        [1, -2],
    ]

    def __init__(self, chess_board):
        self.chess_board = chess_board

    def is_valid_position(self, position):
        return self.chess_board.is_position_inside_board(position)

    def calculate_possible_location(self, coordinate):
        possible_locations = []
        for move in self.possible_moves:
            final_x = coordinate[0] + move[0]
            final_y = coordinate[1] + move[1]

            if self.is_valid_position([final_x, final_y]):
                possible_locations.append([final_x, final_y])
        return possible_locations
