from django.db import models
from django.utils.translation import gettext_lazy as _


class ChessPiece(models.Model):

    class PieceColor(models.TextChoices):
        LIGHT = 'light', _('Light')
        DARK = 'dark', _('Dark')

    class PieceType(models.TextChoices):
        BISHOPS = 'bishop', _('Bishop')
        KING = 'king', _('King')
        KNIGHT = 'knight', _('Knight')
        PAWNS = 'pawn', _('Pawn')
        QUEEN = 'queen', _('Queen')
        ROOKS = 'rook', _('Rook')

    color = models.CharField(
        max_length=5,
        choices=PieceColor.choices
    )
    piece_type = models.CharField(
        max_length=10,
        choices=PieceType.choices)


    def __str__(self):
        return f'{self.get_color_display()} - {self.get_piece_type_display()}'

    class Meta:
        unique_together = ['piece_type', 'color']
