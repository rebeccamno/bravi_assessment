from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from chess_board.chess_board import ChessBoard
from chess_piece.models import ChessPiece
from chess_piece.check_possible_location import CheckPossibleLocation
from chess_board.chess_board import InvalidPositionException, InvalidAlgebricCoordinateException

from .serializers import ChessPieceIdSerializer


class GetPieceId(APIView):
    permission_classes = [AllowAny]

    def get(self, request, format=None):
        color = self.request.GET.get('color')
        piece_type = self.request.GET.get('piece_type')
        chess_piece = get_object_or_404(ChessPiece,
                                        color=color,
                                        piece_type=piece_type)

        return Response(ChessPieceIdSerializer(chess_piece,
                                               many=False,
                                               context={'request': request}
                                               ).data)


class GetPiecePossibleLocation(APIView):
    permission_classes = [AllowAny]

    def get(self, request, pk, format=None):
        coordinate = self.request.GET.get('coordinate')
        chess_piece = get_object_or_404(ChessPiece,
                                        pk=pk)

        try:
            chess_board = ChessBoard(8, 8)
            possible_locations = CheckPossibleLocation(chess_board, chess_piece, coordinate)()
            return Response(possible_locations)
        except InvalidPositionException as e:
            return Response(str(e))
        except InvalidAlgebricCoordinateException as e:
            return Response(str(e))
