from rest_framework import serializers
from chess_piece.models import ChessPiece


class ChessPieceIdSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChessPiece
        fields = ['id', ]
