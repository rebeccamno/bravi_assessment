from django.urls import path

from .views import (
    GetPieceId,
    GetPiecePossibleLocation
)

urlpatterns = [
    path('find-id/', GetPieceId.as_view(),
         name='get_piece_id'),
    path('get-possible-locations/<int:pk>/',
         GetPiecePossibleLocation.as_view(),
         name='get_piece_id')
]
