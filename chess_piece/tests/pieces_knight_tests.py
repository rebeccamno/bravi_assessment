from unittest.mock import MagicMock, patch

from chess_piece.pieces import Knight
from chess_board.chess_board import ChessBoard


def test_knight():
    chess_board_mock = MagicMock()
    knight = Knight(chess_board_mock)
    assert knight.chess_board == chess_board_mock


def test_is_valid_position():
    chess_board_mock = MagicMock()
    is_position_inside_board_mock = MagicMock(return_value=True)
    chess_board_mock.is_position_inside_board = is_position_inside_board_mock

    knight = Knight(chess_board_mock)
    position = [1, 1]
    result = knight.is_valid_position(position)

    assert result is True
    is_position_inside_board_mock.assert_called_with(position)


def test_calculate_possible_location():
    chess_board = ChessBoard(8, 8)

    knight = Knight(chess_board)
    coordinate = [4, 4]
    result = knight.calculate_possible_location(coordinate)
    assert result == [[6, 5], [6, 3], [2, 5], [2, 3], [3, 6], [5, 6], [3, 2], [5, 2]]


def test_calculate_possible_location_coner_bottom_right():
    chess_board = ChessBoard(8, 8)

    knight = Knight(chess_board)
    coordinate = [8, 1]
    result = knight.calculate_possible_location(coordinate)
    assert result == [[6, 2], [7, 3]]


def test_calculate_possible_location_coner_top_right():
    chess_board = ChessBoard(8, 8)

    knight = Knight(chess_board)
    coordinate = [7, 6]
    result = knight.calculate_possible_location(coordinate)
    assert result == [[5, 7], [5, 5], [6, 8], [8, 8], [6, 4], [8, 4]]
