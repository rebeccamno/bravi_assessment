from unittest.mock import MagicMock, patch

from chess_piece.models import ChessPiece
from chess_piece.pieces import Knight
from chess_piece.check_possible_location import CheckPossibleLocation


patch_root = 'chess_piece.check_possible_location'


@patch(f'{patch_root}.CheckPossibleLocation.get_piece_type',
       return_value=MagicMock())
def test_check_possible_location(get_piece_type_mock):
    chess_board_mock = MagicMock(convert_algebric_to_coordinate=MagicMock(return_value=[2,7]))
    chess_piece_mock = MagicMock()
    coordinate = 'b7'
    result = CheckPossibleLocation(chess_board_mock, chess_piece_mock, coordinate)

    get_piece_type_mock.assert_called_with(chess_board_mock, chess_piece_mock)
    chess_board_mock.assert_called_once
    assert result.coordinate == [2, 7]


@patch(f'{patch_root}.Knight',
       return_value=MagicMock())
def test_get_piece_type(knight_mock):
    chess_board_mock = MagicMock(convert_algebric_to_coordinate=MagicMock(return_value=[2,7]))
    chess_piece_mock = MagicMock()
    chess_piece_mock.piece_type = ChessPiece.PieceType.KNIGHT
    coordinate = 'b7'
    cpl = CheckPossibleLocation(chess_board_mock, chess_piece_mock, coordinate)
    result = cpl.get_piece_type(chess_board_mock, chess_piece_mock)

    assert result == knight_mock.return_value


def test_get_piece_type_none():
    chess_board_mock = MagicMock(convert_algebric_to_coordinate=MagicMock(return_value=[2, 7]))
    chess_piece_mock = MagicMock()
    coordinate = 'b7'
    cpl = CheckPossibleLocation(chess_board_mock, chess_piece_mock, coordinate)
    result = cpl.get_piece_type(chess_board_mock, chess_piece_mock)

    assert result is None


@patch(f'{patch_root}.CheckPossibleLocation.get_piece_type',
       return_value=MagicMock())
def test_check_possible_location_call(get_piece_type_mock):
    chess_board_mock = MagicMock(
        convert_algebric_to_coordinate=MagicMock(return_value=[1, 7]),
        get_algebraic_position=MagicMock(return_value='a1'))
    chess_piece_mock = MagicMock()
    coordinate = 'b7'
    cpl = CheckPossibleLocation(chess_board_mock, chess_piece_mock, coordinate)
    cpl.chess_board = chess_board_mock

    calculate_possible_location_mock = MagicMock(return_value=[[2, 2], [4, 4], [5, 5]])
    cpl.piece_type = MagicMock()
    cpl.piece_type.calculate_possible_location = calculate_possible_location_mock
    result = cpl()

    assert result == [{'a1': ['a1', 'a1', 'a1']}, {'a1': ['a1', 'a1', 'a1']}, {'a1': ['a1', 'a1', 'a1']}]
    calculate_possible_location_mock.assert_called_with([1, 7])
