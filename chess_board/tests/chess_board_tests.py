
from pytest import raises
from unittest.mock import MagicMock, patch

from chess_board.chess_board import (ChessBoard, InvalidPositionException,
                                     InvalidAlgebricCoordinateException)


patch_root = 'chess_board.chess_board'


@patch(f'{patch_root}.ChessBoard.generate_algebraic_positions',
       return_value=MagicMock())
def test_chess_board(generate_algebraic_positions_mock):
    chess_board = ChessBoard(8, 9)

    assert chess_board.row_number == 8
    assert chess_board.column_number == 9
    generate_algebraic_positions_mock.assert_called_once


def test_convert_algebric_to_coordinate():
    chess_board = ChessBoard(8, 8)
    result = chess_board.convert_algebric_to_coordinate('b7')

    assert result == [2, 7]

    result = chess_board.convert_algebric_to_coordinate('h1')

    assert result == [8, 1]


def test_convert_algebric_to_coordinate_raises():
    chess_board = ChessBoard(8, 8)

    with raises(InvalidAlgebricCoordinateException) as excinfo:
        chess_board.convert_algebric_to_coordinate('bb')

    error_msg = "Invalid coordinate bb !"

    assert str(excinfo.value) == error_msg


def test_get_algebraic_positions():
    chess_board = ChessBoard(8, 8)
    result = chess_board.generate_algebraic_positions()

    assert result[0][0] == 'a1'
    assert result[7][7] == 'h8'
    assert result[2][5] == 'c6'
    assert result[4][3] == 'e4'
    assert len(result) == 8
    assert len(result[0]) == 8


def test_get_algebraic_position():
    chess_board = ChessBoard(8, 8)
    c1 = chess_board.get_algebraic_position([3, 1])
    g4 = chess_board.get_algebraic_position([7, 4])
    e5 = chess_board.get_algebraic_position([5, 5])
    g3 = chess_board.get_algebraic_position([7, 3])
    h8 = chess_board.get_algebraic_position([8, 8])
    h3 = chess_board.get_algebraic_position([8, 3])

    assert c1 == 'c1'
    assert g4 == 'g4'
    assert e5 == 'e5'
    assert g3 == 'g3'
    assert h8 == 'h8'
    assert h3 == 'h3'


def test_get_algebraic_position_raises():
    chess_board = ChessBoard(8, 8)

    with raises(InvalidPositionException) as excinfo:
        chess_board.get_algebraic_position([9, 8])

    error_msg = "Invalid position [9, 8] !"

    assert str(excinfo.value) == error_msg


def test_is_position_inside_board():
    chess_board = ChessBoard(8, 8)
    result_f = chess_board.is_position_inside_board([0, 0])
    result_t = chess_board.is_position_inside_board([1, 1])

    assert result_f is False
    assert result_t is True

    result_f = chess_board.is_position_inside_board([2, 10])
    result_t = chess_board.is_position_inside_board([2, 8])

    assert result_f is False
    assert result_t is True

    result_f = chess_board.is_position_inside_board([-2, 2])
    result_t = chess_board.is_position_inside_board([8, 8])
