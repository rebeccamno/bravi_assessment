import string


class InvalidAlgebricCoordinateException(BaseException):
    pass


class InvalidPositionException(BaseException):
    pass


class ChessBoard:

    def __init__(self, row_number, column_number):
        self.row_number = row_number
        self.column_number = column_number
        self.algebraic_positions = self.generate_algebraic_positions()

    @staticmethod
    def convert_algebric_to_coordinate(algebraic_position):
        '''
            convert algebraic position 'a1', 'b1'
            to x,y coordinate [0,0], [1,0]
        '''
        try:
            x = string.ascii_lowercase.index(algebraic_position[0].lower()) + 1
            y = int(algebraic_position[1])
            return [x, y]
        except Exception as e:
            raise InvalidAlgebricCoordinateException(f"Invalid coordinate {algebraic_position} !")

    def generate_algebraic_positions(self):
        alphabet_string = string.ascii_lowercase
        chess_board_positions = []
        for column in range(self.column_number):
            items = []
            for row in range(1, self.row_number + 1):
                items.append(f"{alphabet_string[column]}{row}")

            chess_board_positions.append(items)
        return chess_board_positions

    def get_algebraic_position(self, coordinate):
        try:
            x = coordinate[0] - 1
            y = coordinate[1] - 1
            return self.algebraic_positions[x][y]
        except Exception:
            raise InvalidPositionException(f'Invalid position {coordinate} !')

    def is_position_inside_board(self, coordinate):
        x = coordinate[0]
        y = coordinate[1]
        if x > self.column_number or y > self.row_number or x < 1 or y < 1:
            return False

        return True
